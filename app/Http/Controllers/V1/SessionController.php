<?php

namespace App\Http\Controllers\V1;

use App\Exceptions\User\IncorrectPasswordException;
use App\Exceptions\User\UserNotFoundException;
use App\Http\Controllers\Controller;
use App\Http\Resources\User\UserResource;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class SessionController extends Controller
{
	public function index(Request $request)
	{
		
	}

    public function login(Request $request)
    {
    	$user = User::where("email",$request->email)->first();
    	if(!$user) {
    		throw new UserNotFoundException;
    	} else{
    		if(Hash::check($request->password,$user->password)){
	    		$passport_token = $user->createToken($user->device_id);
	    		$access_token = $passport_token->accessToken;
	    		$expires_at = $passport_token->token->expires_at->toDayDateTimeString();
    			return [
    				'user' => new UserResource($user),
    				'credential' => [
    					'access_token' => $access_token,
    					'expires_at' => $expires_at
    				]
    			];
    		}else{
    			throw new IncorrectPasswordException;
    		}
    	}
    }

    public function refreshToken(Request $request)
    {

    }
}
