<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Resources\Json\Resource;

class UserResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'approved' => ($this->approved == 1) ? true : false,
            'is_banned' => ($this->is_banned == 1) ? true : false,
            'is_deleted' => ($this->is_deleted == 1) ? true : false
        ];
    }
}

/**
 "id": 43,
                "name": "Minerva Wilkinson Sr.",
                "email": "beahan.bertha@example.com",
                "approved": 0,
                "is_banned": 1,
                "is_deleted": 1
*/
