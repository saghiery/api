<?php

namespace App\Http\Resources\Device;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\User\UserResource;

class DeviceCollection extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
         return [
            'device_id' => $this->device_id,
            'os' => $this->os,
            'push_token' => $this->push_token,
            'user' => new UserResource($this->user)
        ];
    }
}
