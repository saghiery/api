<?php

namespace App\Exceptions\User;

use App\Exceptions\ExceptionTrait;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class UserNotFoundException extends Exception
{
    use ExceptionTrait;
    public function render()
    {
    	return $this->renderException(3,'UserNotFoundException',"Please make sure you've entered correct Email/Passwprd",Response::HTTP_NOT_FOUND);
    }
}
