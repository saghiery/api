<?php

namespace App\Exceptions\User;

use App\Exceptions\ExceptionTrait;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class IncorrectPasswordException extends Exception
{
    use ExceptionTrait;
    public function render()
    {
    	return $this->renderException(3,'IncorrectPasswordException',"You Have Entered Incorrect Password",Response::HTTP_NOT_FOUND);
    }
}
