<?php 
namespace App\Exceptions;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Exception;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

trait ExceptionTrait{
	public function apiException($request,Exception $exception)
	{
		if($exception instanceof ModelNotFoundException){
			return $this->renderException(1,'ModelNotFoundException','Model Not Found',Response::HTTP_NOT_FOUND);
			    // return response()->json(['error' => 'ModelNotFoundException'],Response::HTTP_NOT_FOUND);
        }
        if ($exception instanceof NotFoundHttpException) {
        	return $this->renderException(2,'NotFoundHttpException','Route Not Found',Response::HTTP_NOT_FOUND);
                // return response()->json(['error' => 'NotFoundHttpException'],Response::HTTP_NOT_FOUND);
        }

        return parent::render($request, $exception);
	}

	public function renderException($code,$name,$message,Int $http_response_code)
	{
		$exceptionJson = ['code' => $code, 'name' => $name, 'message' =>$message];
		return response()->json(['error' => $exceptionJson],$http_response_code);
	}
}

?>