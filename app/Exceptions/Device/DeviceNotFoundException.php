<?php

namespace App\Exceptions\Device;

use App\Exceptions\ExceptionTrait;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class DeviceNotFoundException extends Exception
{
	use ExceptionTrait;

    public function render(){
    	return $this->renderException(11,'DeviceNotFoundException','No Device was registered using this ID',Response::HTTP_NOT_FOUND);
    	// return response()->json(['error' => 'Device Not Found'],Response::HTTP_NOT_FOUND);
    }
}
