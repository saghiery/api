<?php

namespace App\Exceptions\Device;

use Exception;
use Symfony\Component\HttpFoundation\Response;

class DeviceNotFound extends Exception
{
    public function render(){
    	return response()->json(['error' => 'Device Not Found'],Response::HTTP_NOT_FOUND);
    }
}
