<?php

use Faker\Generator as Faker;
use App\User;

$factory->define(App\Model\Device::class, function (Faker $faker) {
    return [
        'user_id' => function() {
        	return User::all()->random();
        },
        'device_id' => str_random(24),
        'os' => $faker->randomElement(['android','ios','web']),
        'device_type' => $faker->randomElement(['phone','tablet','web']),
        'os_version' => $faker->numberBetween(1,5),
        'app_version' => $faker->numberBetween(1,5),
        'push_token' => str_random(16),
        'info' => $faker->word
    ];
});