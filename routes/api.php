<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::apiResource('/device','DeviceController');
Route::apiResource('/user','UserController');
Route::group(['prefix' => 'V1/session'],function(){
	Route::post('/','V1\SessionController@index');
	Route::post('/','V1\SessionController@login');
	Route::put('/','App\Http\Controllers\V1\SessionController@refreshToken');
});
